# **Dolibarr**

The purpose of the Dolibarr module is to integrate Drupal with the excellent open source [Dolibarr ERP & CRM](https://www.dolibarr.org) system. In its current iteration, the primary focus of this module is to sync products into Drupal Commerce. With this module, you can create a fully functional, robust e-Commerce store based on products stored in your Dolibarr instance.

## **Table of Contents**

- Requirements

- Recommendations

- Installation

- Configuration

- Troubleshooting

- Maintainers

## **Requirements**

- Dolibarr 14.x up to 17.0.2  (older versions untested)

- Drupal 9.3.x (older versions and Drupal 10 untested)

- Open SQL port to Dolibarr server (local or remote)

- [Centarro Commerce Kickstart 3.x template](https://github.com/centarro/commerce-kickstart-project)

- SQL account with read-only access to Dolibarr database

- Dolibarr account account with proper API access

- [Commerce Core](https://www.drupal.org/project/commerce)^2

- [Migrate Plus](https://www.drupal.org/project/migrate_plus)^6

- [Commerce Kickstart](https://www.drupal.org/project/commerce_kickstart)^3

- [Commerce Migrate](https://www.drupal.org/project/commerce_migrate)^3.2

- [Migrate Magician](https://www.drupal.org/project/migmag)^1.8

- [Migrate Conditions](https://www.drupal.org/project/migrate_conditions)^2

- [Drupal Group](https://www.drupal.org/project/group)^3

- [Migrate Files (extended)](https://www.drupal.org/project/migrate_file)^1.2

- [Custom Migrate SQL Source Plugin](https://www.drupal.org/project/custom_sql_migrate_source_plugin)^1.3

- [Drush](https://www.drupal.org/project/drush)^11.2

- [Composer Patches](https://github.com/cweagans/composer-patches)^1.7

## **Recommendations**

- Dolibarr development instance cloned from production

## **Installation**

### **Centarro Commerce Kickstart Template**

- The [README](https://github.com/centarro/commerce-kickstart-project/blob/3.x/README.md) in the Commerce Kickstart template has instructions on how to start using the template.

- During the select features part of of setup. Select every box under the **Select starting features**.

- This must be done when first creating the drupal site.

- The template was recently updated to work with Drupal 10, so the composer.json file will need all references to Drupal 10 changed to ^9.1. Delete the composer.lock file and then run composer install.

### **Dolibarr Module Installation**

- The module will require a store to be created. Currently only one store is supported.

- Composer will be used to install any required modules that were not installed when initializing the site using the Kickstart Template.

  ``` text
    composer require cweagans/composer-patches

    composer require 'drupal/commerce:^2.36'

    composer require 'drupal/commerce_migrate:^3.2'

    composer require 'drupal/custom_sql_migrate_source_plugin:^1.3'

    composer require 'drupal/group:^3.1'

    composer require 'drupal/migmag:^1.8'

    composer require 'drupal/migrate_conditions:^2.1'

    composer require 'drupal/migrate_file:^2.1'

    composer require 'drupal/migrate_plus:^6.0'
  ```

### **Migration**

#### **Dolibarr SQL**

- The SQL migration portion will require the `settings.local.php` file to be moved to `/home/foo/bar/myDrupalSite/web/sites/default`. It currently resides in the main module folder. The file contains the code below, but will need the details filled in. The login and ip information are for the SQL database that Dolibarr is using.

  ``` php
    <?php

  /**
  * Database settings:
   */
  //dev_remote
  $databases['dolibarr']['default'] = array (
    'database'  => 'DATABASE_NAME_HERE',
    'username'  => 'YOUR_USERNAME_HERE',
    'password'  => 'YOUR_PASSWORD_HERE',
    'prefix'    => 'llxtk_',
    'host'      => 'localhost',
    'port'      => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver'    => 'mysql',
  );
  ```

#### **Tags and Categories**

- The migration will be looking for a custom checkbox in Dolibarr, so that it knows which products to migrate to the Drupal site. The check box can be titled anything, but the 'Attribute Code' is currently required to match `online_store_sale`.

- This command should be ran first before running the migration script or creating the fields and facets. This is part of the process for creating the tags and categories.

    ``` text
    drush cim --partial --source=modules/contrib/dolibarr/config/post_install --yes
    ```

- Navigate to the product types, `Home>Administration>Commerce>Configuration>Product types`, and select to edit the physical product type. Select "Manage Fields", and then click the button to add a field. Search for the option that says `Entity reference: dolibarr_tags_categories`. The label should be set to `Dolibarr Tags & Categories`. Make sure that the `Dolibarr Tags & Categories` in the `Vocabulary` is checked.

- Next, head on over to `Home>Administration>Configuration>Search and metadata>Search API>Products` and go to the `Fields` tab. Click the `Add fields` button, and look for the `dolibarr_tags_categories`. Click add, select done, and then save.

- Now go to `Home>Administration>Configuration>Search and metadata>Facets`, and click the button to add a facet.
  - `Facet Source -> View Product catalog, display Product Catalog Page`
  - `Field -> Dolibarr Tags $ Categories (dolibarr_tags_categories)`
  - `Name -> Dolibarr Tags & Categories`

- The image migration requires a folder to be created in `web/sites/default/files`. The folder should be named `dolibarr_product_images`. A photo needs to be placed into the folder called `nophoto.png`. This will be used for products that do not have an image imported from Dolibarr.

- Two of the migrations are dependant upon a user and a group type being created in drupal.

  - The customer migration is set to tie all contact information to one user, and will show the information under profiles. The user name should be set to: `dolibarr_profile_owner`

  - Group Type Name: `Dolibarr Third Party`. This should make the machine readable name `dolibarr_third_party`

  - When creating the group type check the following options:

    - The group creator automatically becomes a member
    - Automatically configure default useful roles
    - Automatically configure an administrative role
    - **UNCHECK** Group creator must complete their membership.

- There is a script included that can be used to schedule the migration. The file `dolisync_mig_only.sh` will require a couple steps to use.

  - The script will need to be placed in `/usr/local/sbin`

  - The directory of the project will need to be entered. The script currently shows: `cd /home/foo/bar/myDrupalSite`

- The script also contains the name of all the migrations, and can be used to run them individually.

- If running the migrations separately then this command will be required to be ran after completion. The script will do this automatically.

  ``` text
  drush cim --partial --source=modules/contrib/dolibarr/config/post_mig --yes
  ```

## **Configuration**

### Dolibarr

- The shipping module must be set to `Safor`.

### Drupal

- Settings link can be found in `administration>Configuration>Development>Dolibarr`

- All fields need to be filled out every time. Currently working on problem with empty fields erasing values when submitted.

- Users must be required to validate by link before being able to access their account. The users are assigned to the correct group when the account validation is completed.

## **Troubleshooting**

### **Known Issues**

- SQL errors are displayed in migration console during image sync.

- Some product images don't sync properly and require a delete and reupload in Dolibarr.

- <https://www.drupal.org/project/commerce/issues/3127087#comment-15049023> requires a Registration link workaround

- After changing any API information in the settings a cache rebuild is required.

- The migration file `dolibarr_product_variation.yml` is set for the Dolibarr database prefix to be 'llx_'. If the instance of Dolibarr being used has been setup with a different prefix this file must be manually changed.

- Existing carts must be deleted after running a product migration, so that a null value error can be avoided.

- If groups are rolled back and reimported the link is lost between user and group. This will result in orders being posted to the default online order account in Dolibarr.

## **Maintainers**

- [Red Ridge Digital](https://www.drupal.org/red-ridge-digital)

- [Harlan Gross](https://www.drupal.org/u/montag64)

- [Allen Nicholson](https://www.drupal.org/u/allun55)
