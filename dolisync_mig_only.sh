# Change path to match project directory.
cd /home/foo/bar/myDrupalSite
# Reset the migration process. This is done in case a previous migration failed.
drush migrate:reset dolibarr_product_image_bind_none
drush migrate:reset dolibarr_product_image_bind
drush migrate:reset dolibarr_product_image_save
drush migrate:reset dolibarr_product_image_fetch
drush migrate:reset dolibarr_product_term
drush migrate:reset dolibarr_product
drush migrate:reset dolibarr_product_variation
drush migrate:reset dolibarr_product_variation_type
drush migrate:reset dolibarr_product_attribute_value
drush migrate:reset dolibarr_product_attribute
drush migrate:reset dolibarr_category_term
drush migrate:reset dolibarr_category
drush migrate:reset dolibarr_customer
drush migrate:reset dolibarr_company
# Rollback what was previously migrated. This is done in case an item is removed from Dolibarr, and will now
# need to be removed from the site.
drush migrate:rollback dolibarr_product_image_bind_none
drush migrate:rollback dolibarr_product_image_bind
drush migrate:rollback dolibarr_product_image_save
drush migrate:rollback dolibarr_product_image_fetch
drush migrate:rollback dolibarr_product_term
drush migrate:rollback dolibarr_product
drush migrate:rollback dolibarr_product_variation
drush migrate:rollback dolibarr_product_variation_type
drush migrate:rollback dolibarr_product_attribute_value
drush migrate:rollback dolibarr_product_attribute
drush migrate:rollback dolibarr_category_term
drush migrate:rollback dolibarr_category 
# This will need to be disabled after the first migration. The link between accounts and groups
# is destroyed when the groups are rolled back.
drush migrate:rollback dolibarr_company
drush migrate:rollback dolibarr_customer
# Run the imports that begin migration. This needs to stay in a specific order.
drush migrate:import dolibarr_customer
drush migrate:import dolibarr_company
drush migrate:import dolibarr_category
drush migrate:import dolibarr_category_term
drush migrate:import dolibarr_product_attribute
drush migrate:import dolibarr_product_attribute_value
drush migrate:import dolibarr_product_variation_type
drush migrate:import dolibarr_product_variation
drush migrate:import dolibarr_product
drush migrate:import dolibarr_product_term
drush migrate:import dolibarr_product_image_fetch
drush migrate:import dolibarr_product_image_save
drush migrate:import dolibarr_product_image_bind
drush migrate:import dolibarr_product_image_bind_none
# Checks a box required for taxonomy.
drush cim --partial --source=modules/contrib/dolibarr/config/post_mig --yes