<?php

/**
* Database settings:
 */
//dev_remote
$databases['dolibarr']['default'] = array (
  'database'  => 'DATABASE_NAME_HERE',
  'username'  => 'YOUR_USERNAME_HERE',
  'password'  => 'YOUR_PASSWORD_HERE',
  'prefix'    => 'llxtk_',
  'host'      => 'localhost',
  'port'      => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver'    => 'mysql',
);
