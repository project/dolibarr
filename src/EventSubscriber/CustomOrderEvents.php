<?php

declare(strict_types=1);

namespace Drupal\Dolibarr\EventSubscriber;

use Drupal;
use GuzzleHttp\Client;
use Symfony\Component\Mailer\Mailer;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to handle Dolibarr order and shipping
 *
 * @package \Drupal\Dolibarr\EventSubscriber
 */

class CustomOrderEvents implements EventSubscriberInterface
{

  use StringTranslationTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManager;

  /**
   * Constructor for MymoduleServiceExample.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(Client $http_client, TranslationInterface $string_translation, EntityTypeManager $entityTypeManager)
  {
    $this->httpClient = $http_client;
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Returns the default http client.
   *
   * @return\GuzzleHttp\ClientInterface
   *   A guzzle http client instance.
   */
  public static function httpClient()
  {
    return static::getContainer()->get('http_client');
  }

  // Monitor events for an order transitioning to the 'completed' state.
  public static function getSubscribedEvents()
  {
    $events = [
      'commerce_order.place.post_transition' => ['dolibarrOrderProcess', 100],
    ];

    return $events;
  }

  // This function is called when an order is placed and the state is changed to 'completed'
  public function dolibarrOrderProcess(WorkflowTransitionEvent $event)
  {
    // Pulling date and API key from config
    $date = date('Y-m-d H:i:s');
    $dolibarrAPIKey = getAPIToken();

    // This is the client that will be used to make the API calls to Dolibarr
    $doliClient = new Client(['headers' => ['DOLAPIKEY' => $dolibarrAPIKey]]);

    // Total price is pulled from the order
    $totalPrice = $event->getEntity()->getTotalPrice()->getNumber();

    // Using to determine tax amount. Tax was not falling under adjustments which is why this is needed.
    $taxAmount = $totalPrice;

    // This pulls the order id from the order.
    $drupalOrderID = (int)$event->getEntity()->id();
    $socID = NULL;

    // This pulls the user id and then looks up the group that the user is tied to. The response is placed into an
    // array and then the id of the group is pulled which is the socid
    $uid = $event->getEntity()->get('uid')->target_id;
    $user = \Drupal\user\Entity\User::load($uid);
    $group_memberships = \Drupal::service('group.membership_loader')->loadByUser($user);
    $groups = [];
    foreach ($group_memberships as $group_membership) {
      $groups[] = $group_membership->getGroup();
    }

    // If the length of the array is longer than 1 we will need to check dolibarr to match zipcode to correct group.
    $arraySize = count($groups);
    if ($arraySize > 1) {
      try {
        //Call to dolibarr that returns a list of all contacts.
        $dolibarrContactsCall = $doliClient->get(getAPIUrlBase() . '/contacts?sortfield=t.rowid&sortorder=ASC&limit=0');
      } catch (\Exception $error) {
        $this->errorSendEmail($drupalOrderID, "Group Selection", (string) $error);
        return (-1);
      }
      // Convert API response to an array
      $doliContactsResponse = json_decode((string) $dolibarrContactsCall->getBody(), true);
      // This pulls the email address from the user object.
      $email = $user->get('mail')->getString();
      // This pulls the zipcode from the order.
      $shipment = $event->getEntity()->get('shipments')->referencedEntities();
      $zipCode = $shipment[0]->getShippingProfile()->address->postal_code;

      // This finds all contacts with matching email address, and then cross references zip code to get correct socid.
      foreach ($doliContactsResponse as $contact) {
        $contactEmail = $contact['email'];
        $contactZipCode = $contact['zip'];
        if ($contactEmail == $email) {
          $socID = $contact['socid'];
          if ($zipCode == $contactZipCode) {
            $socID = $contact['socid'];
            break;
          }
        }
      }
    } else {
      if (isset($groups[0])) {
        $socID = $groups[0]->id();
      } else {
        $error =  "Cannot pull SocID becuase user with id $uid does not have a group membership. Most likely was API error on initial user registration, or group does not exist.";
        $this->errorSendEmail($drupalOrderID, "Third Party ID Retrieval from Group Membership", (string) $error);
      }
    }

    if ($socID == NULL) {
      // This is guest checkout if no other socid is found.
      $socID = getDolibarrCode('guest');
    }

    // These are the options to create the order. Items will be added by line in a separate call.
    $orderOptions = [
      'form_params' => [
        'socid' => $socID,
        'date' => $date,
        'type' => 0,
      ],
    ];

    // This is the POST the creates the order
    try {
      $response = $doliClient->post(getAPIUrlBase() . '/orders', $orderOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Order Creation", (string) $error);
      return (-1);
    }

    // This pulls the order number from the Dolibarr response
    $responseOrderNumber = (int) json_decode((string) $response->getBody());

    //For loop to add all items to order.
    $orderItems = $event->getEntity()->getItems();
    foreach ($orderItems as $order_item) {
      $orderItemID = $order_item->getPurchasedEntity()->get('product_id')[0]->get('target_id')->getValue();
      $orderQuantity = (int) $order_item->get('quantity')[0]->getString();
      // The cost of each item is subtracted from taxAmount. taxAmount started as total cost of order, and will end as cost of tax.
      $orderItemCost = $order_item->getPurchasedEntity()->getPrice()->getNumber();
      $taxAmount -= ($orderItemCost * $orderQuantity);
      $lineItemOptions = [
        'form_params' => [
          'fk_product' => $orderItemID,
          'qty' => $orderQuantity,
          'subprice' => $orderItemCost
        ]
      ];

      // This call adds the item to the order.
      try {
        $doliClient->post(getAPIUrlBase() . "/orders/$responseOrderNumber/lines", $lineItemOptions);
      } catch (\Exception $error) {
        $this->errorSendEmail($drupalOrderID, "Order Item Add", (string) $error, $responseOrderNumber);
        return (-1);
      }
    }

    // This call validates the order after all items have been added.
    try {
      $doliClient->post(getAPIUrlBase() . "/orders/$responseOrderNumber/validate");
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Order Validation", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // Pull the warehouse id
    $warehouse = getDolibarrCode('warehouse');
    // This call creates a shipment for the order
    try {
      $shipmentResponse = $doliClient->post(getAPIUrlBase() . "/orders/$responseOrderNumber/shipment/$warehouse");
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Shipment Creation", (string) $error, $responseOrderNumber);
      return (-1);
    }
    // This pulls the shipment number from the Dolibarr response
    $responseShipmentNumber = (int) json_decode((string) $shipmentResponse->getBody());
    $shipValidationOptions = [
      'form_params' => [
        'notrigger' => 1
      ]
    ];

    // This call validates the shipment for the order
    try {
      $doliClient->post(getAPIUrlBase() . "/shipments/$responseShipmentNumber/validate", $shipValidationOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Shipment Validation", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // This call generates an invoice in Dolibarr using the order number returned from the API call creating the order.
    try {
      $invoiceResponse = $doliClient->post(getAPIUrlBase() . "/invoices/createfromorder/$responseOrderNumber");
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Invoice Creation", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // This is pulling the invoice number from the response.
    $invoiceResponseIDArray = json_decode((string) $invoiceResponse->getBody(), true);
    $invoiceResponseID = $invoiceResponseIDArray['id'];

    // The drupal order number is required to pull the payment information.
    // Payment information pulled from Drupal database.
    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties(['order_id' => $drupalOrderID]);
    // This pulls the array key to find the returned payment informaiton from above.
    $arrayKey = key($payments);
    // Confirmed that this works with Stripe payments. Not sure if it will work with other payment methods.
    $paymentID = $payments[$arrayKey]->getRemoteId();
    $datePay = (new DrupalDateTime())->getTimestamp();

    // Pulls the shipping cost out of the adjustments
    $adjustments = $event->getEntity();
    $orderShippingCost = NULL;

    // This foreach loop pulls the shipping cost and service fee from the adjustments.
    foreach ($adjustments->collectAdjustments() as $adjustment) {
      if ($adjustment->getType() == 'shipping') {
        $orderShippingCost = $adjustment->getAmount()->getNumber();
      } else if ($orderShippingCost == NULL) {
        $orderShippingCost = 0;
      }
      if ($adjustment->getType() == 'fee') {
        $orderFeeCost = $adjustment->getAmount()->getNumber();
      }
    }

    // Call function in dolibarr.module to get the product code for shipping.
    $shippingCode = getDolibarrCode('shipping');
    $orderShipCostOptions = [
      'form_params' => [
        'desc' => 'Online Order Shipping Charge',
        'fk_product' => $shippingCode,
        'qty' => '1',
        'subprice' => $orderShippingCost
      ]
    ];

    // Adds the shipping cost to the invoice.
    try {
      $doliClient->post(getAPIUrlBase() . "/invoices/$invoiceResponseID/lines", $orderShipCostOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Invoice Line Item Add Shipping", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // Round fee to 2 decimal places.
    $orderFeeCost = round((float) $orderFeeCost, 2);
    // Call function from dolibarr.module to get the product code for the order fee.
    $orderFeeCode = getDolibarrCode('orderFee');
    // Options 
    $orderFeeCostOptions = [
      'form_params' => [
        'desc' => 'Online Order Processing Fee',
        'fk_product' => $orderFeeCode,
        'qty' => '1',
        'subprice' => $orderFeeCost
      ]
    ];

    // Adds the order fee to the invoice.
    try {
      $doliClient->post(getAPIUrlBase() . "/invoices/$invoiceResponseID/lines", $orderFeeCostOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Invoice Line Item Add Processing Fee", (string) $error, $responseOrderNumber);
      return (-1);
    }

    //Calculating tax amount
    $taxAmount -= $orderShippingCost;
    $taxAmount -= $orderFeeCost;
    // Call function from doliarr.module to get the product code for tax.
    $taxCode = getDolibarrCode('tax');

    // Options for adding tax line item to invoice.
    $orderTaxCostOptions = [
      'form_params' => [
        'desc' => 'Online Order Tax',
        'fk_product' => $taxCode,
        'qty' => '1',
        'subprice' => $taxAmount
      ]
    ];

    // Adds the tax amount to the invoice.
    try {
      $doliClient->post(getAPIUrlBase() . "/invoices/$invoiceResponseID/lines", $orderTaxCostOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Invoice Line Item Add Tax", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // paymentid = The Dolibarr database has the id of "Online Payment" set to 50.
    // accountid = Dolibarr assigned id# for the primary "bank" that the payment is credited to. Function in dolibarr.module pulls this id.
    $accountID = getDolibarrCode('payment');
    // num_payment = The payment confirmation that is returned by the payment system
    // invoiceResponseID = Invoice number generated when creating the invoice in a previous API call.
    // totalPrice = Total amount that is charged when payment is processed.
    // Options for invoice payment post.
    $paymentPostOptions = [
      'form_params' => [
        'datepaye' => $datePay,
        'paymentid' => 50,
        'closepaidinvoices' => "no",
        'accountid' => $accountID,
        'comment' => 'Online payment from completed order on website.',
        'num_payment' => $paymentID,
        'arrayofamounts' => [
          $invoiceResponseID => [
            'amount' => $totalPrice
          ]
        ]
      ]
    ];

    // This call posts a payment to the invoice generated in a previous call.
    try {
      $doliClient->post(getAPIUrlBase() . "/invoices/paymentsdistributed", $paymentPostOptions);
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Invoice Post Payment", (string) $error, $responseOrderNumber);
      return (-1);
    }

    // This call sets the order as having an invoice generated.
    try {
      $doliClient->post(getAPIUrlBase() . "/orders/$responseOrderNumber/setinvoiced");
    } catch (\Exception $error) {
      $this->errorSendEmail($drupalOrderID, "Order Set Billed", (string) $error, $responseOrderNumber);
      return (-1);
    }
  }

  public function errorSendEmail(int $drupalOrderNumber, string $errorLocation, string $error, int $dolibarrOrderNumber = NULL)
  {
    $emailFactory = Drupal::service('email_factory');
    $to = getDolibarrCode('email');
    $body = "Drupal order:$drupalOrderNumber failed at $errorLocation API call.";

    if ($dolibarrOrderNumber) {
      $body = $body . "The Dolibarr order number is $dolibarrOrderNumber";
    }

    $body = $body . $error;

    $emailFactory->newTypedEmail('dolibarr', 'default')->setSubject("DRUPAL ORDER:$drupalOrderNumber FAILED TO POST TO DOLIBARR API")->setTo($to)->setBody($body)->send();

    return (1);
  }
}
