<?php

declare(strict_types=1);

namespace Drupal\Dolibarr\EventSubscriber;

use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Proof of concept for event handling
 *
 * @package \Drupal\Dolibarr\EventSubscriber
 */

class CustomCartEvents implements EventSubscriberInterface
{

  use StringTranslationTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructor for MymoduleServiceExample.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(ClientInterface $http_client, TranslationInterface $string_translation, CartManagerInterface $cart_manager)
  {
    $this->httpClient = $http_client;
    $this->stringTranslation = $string_translation;
    $this->cartManager = $cart_manager;
  }

  /**
   * Returns the default http client.
   *
   * @return\GuzzleHttp\ClientInterface
   *   A guzzle http client instance.
   */
  public static function httpClient()
  {
    return static::getContainer()->get('http_client');
  }

  /** This function is called when an event is dispatched.
   * @param \Symfony\Component\EventDispatcher\Event $event
   * 
   * @return\ Events
   */
  public static function getSubscribedEvents()
  {
    // Checks inventory when item is added to cart.
    $events[CartEvents::CART_ENTITY_ADD][] = ['checkInventoryLevel', 100];

    // Checks inventory when item is updated in cart.
    $events[CartEvents::CART_ORDER_ITEM_UPDATE][] = ['checkInventoryLevelOnUpdate', 99];

    return $events;
  }

  /**
   * Checks inventory level when item is added to cart.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $events
   *   The cart entity add event.
   */
  public function checkInventoryLevel(CartEntityAddEvent $events)
  {
    // Gets current store.
    $current_store = \Drupal::service('commerce_store.current_store');
    $store = $current_store->getStore();

    // Gets current cart.
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    $cart = $cart_provider->getCart("default", $store);

    // Gets product ID and price.
    $productID = $events->getEntity()->get('product_id')[0]->get('target_id')->getValue();
    $order_item = $events->getOrderItem();
    $priceCheck = $order_item->getUnitPrice()->getNumber();

    // Gets warehouse ID by calling function in dolibarr.module
    $warehouse = getDolibarrCode('warehouse');

    // Gets inventory level from Dolibarr API.
    $doliResponse = \Drupal::httpClient()->get(getAPIUrlBase() . "/products/$productID/stock?selected_warehouse_id=$warehouse", [
      'headers' => ['DOLAPIKEY' => getAPIToken()],
      'http_errors' => false,
    ]);

    // Checks for errors in Dolibarr API response.
    $doliErrorCheck = json_decode((string) $doliResponse->getBody(), true);

    # Isset checks if error exists in the array to avoid error when items are in stock.
    if (isset($doliErrorCheck['error']) or $priceCheck == 0) {
      if ($priceCheck == 0) {
        \Drupal::logger("dolibarr")->notice("Price is $0.00 on product: $productID");
        \Drupal::messenger()->addMessage("(P001) Item is not available for online sale.");
      } else if ($doliErrorCheck['error']['code'] == '404') {
        \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 404 ERROR");
        \Drupal::messenger()->addMessage("(404) Item is not available for online sale.");
      } else if ($doliErrorCheck['error']['code'] == '401') {
        \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 401 ERROR - NOT AUTHORIZED");
        \Drupal::messenger()->addMessage("(401) Item is not available for online sale.");
      } else if ($doliErrorCheck['error']['code'] == '500') {
        \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 500 SYSTEM ERROR");
        \Drupal::messenger()->addMessage("(500) Item is not available for online sale.");
      } else {
        \Drupal::logger("dolibarr")->notice("(S001) Something went the Dolibarr API call when an item was added to the cart.");
        \Drupal::messenger()->addMessage("(S001) Item is not available for online sale.");
      }

      // Removes item from cart if unable to verify inventory level from Dolibarr.
      $this->cartManager->removeOrderItem($cart, $order_item);
    } else {
      if ($cart->getItems() == NULL) {
        // Pull quantity that is being added to cart.
        $originalQuantity = $events->getOrderItem()->getQuantity();
        $quantity = (int) $events->getOrderItem()->getQuantity();

        // Pull quantity available from Dolibarr API response.
        $quantityAvailable = (int) json_decode((string) $doliResponse->getBody(), true)['stock_warehouses'][$warehouse]['real'];

        // If quantity is greater than quantity available, set quantity to quantity available.
        if ($quantity > $quantityAvailable) {
          $title = $order_item->get('title')[0]->getString();
          $events->getOrderItem()->setQuantity($quantityAvailable);
          $this->cartManager->updateOrderItem($cart, $order_item);
          $quantity = $quantityAvailable;
          \Drupal::messenger()->addMessage("We're sorry, but the quantity ($originalQuantity) you tried to order for $title exceeds what is in stock. We have $quantityAvailable in stock and have updated your cart to reflect that.");
        }
      }

      // If cart is not empty, check inventory level for each item in cart.
      foreach ($cart->getItems() as $order_item) {
        // Pull quantity that is in cart.
        $quantity = (int) $order_item->getQuantity();

        // Pull inventory level from Dolibarr API response.
        $quantityAvailable = (int) json_decode((string) $doliResponse->getBody(), true)['stock_warehouses'][$warehouse]['real'];

        // Pull product name from cart.
        $title = $order_item->get('title')[0]->getString();
        // Pull product ID from cart.
        $orderItemID = $order_item->getPurchasedEntity()->get('product_id')[0]->get('target_id')->getValue();

        // Check if the item pulled from the cart is the item that is currently being added to the cart.
        if ($productID == $orderItemID && $quantity > $quantityAvailable) {
          \Drupal::messenger()->addMessage("We're sorry, but the quantity ($quantity) you tried to order for $title exceeds what is in stock. We have $quantityAvailable in stock and have updated your cart to reflect that.");
          $quantity = $order_item->get('quantity')[0] = $quantityAvailable;
        }
      }
      // Message to display when item is added to cart.
      \Drupal::messenger()->addMessage($this->t('@quantity <i> @entity </i> added to <a href=":url">the cart</a>.', [
        '@quantity' => $quantity,
        '@entity' => $events->getEntity()->label(),
        ':url' => Url::fromRoute('commerce_cart.page')->toString(),
      ]));
    }
  }

  public function checkInventoryLevelOnUpdate(CartOrderItemUpdateEvent $events)
  {
    // Get current store and cart.
    $current_store = \Drupal::service('commerce_store.current_store');
    $store = $current_store->getStore();
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    $cart = $cart_provider->getCart("default", $store);

    // Get warehouse ID by calling function in dolibarr.module
    $warehouse = getDolibarrCode('warehouse');
    foreach ($cart->getItems() as $order_item) {

      // Get the product ID and quantity of the item being added to the cart
      $productID = $order_item->getPurchasedEntity()->get('product_id')[0]->get('target_id')->getValue();
      $quantity = (int) $order_item->get('quantity')[0]->getString();

      // Get the product name from the cart
      $title = $order_item->get('title')[0]->getString();

      // API call to get inventory level of product
      $doliResponse = \Drupal::httpClient()->get(getAPIUrlBase() . "/products/$productID/stock?selected_warehouse_id=$warehouse", [
        'headers' => ['DOLAPIKEY' => getAPIToken()],
        'http_errors' => false,
      ]);

      // Decode the json that is returned from the API call
      $doliResponseBody = json_decode((string) $doliResponse->getBody(), true);

      // Check for errors in the API call
      if (isset($doliResponseBody['error'])) {
        $order_item->setQuantity(0);
        if ($doliResponseBody['error']['code'] == '404') {
          \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 404 ERROR.");
          \Drupal::messenger()->addMessage("(404) $title is out of stock.");
        } else if ($doliResponseBody['error']['code'] == '401') {
          \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 401 ERROR - NOT AUTHORIZED");
          \Drupal::messenger()->addMessage("(401) $title is not available for online sale.");
        } else if ($doliResponseBody['error']['code'] == '500') {
          \Drupal::logger("dolibarr")->notice("DOLIBARR API REPORTING 500 SYSTEM ERROR");
          \Drupal::messenger()->addMessage("(500) $title is not available for online sale.");
        } else {
          \Drupal::logger("dolibarr")->notice("(S001) Something went the Dolibarr API call when an item was updated in the cart.");
          \Drupal::messenger()->addMessage("(S001) $title is not available for online sale.");
        }
      } else {
        // Pull quantity available from Dolibarr API response.
        $quantityAvailable = (int) $doliResponseBody['stock_warehouses'][$warehouse]['real'];

        // If quantity is greater than quantity available, set quantity to quantity available.
        if ($quantity > $quantityAvailable) {
          \Drupal::messenger()->addMessage("We're sorry, but the quantity ($quantity) you tried to order for $title exceeds what is in stock. We have $quantityAvailable in stock and have updated your cart to reflect that.");
          $order_item->setQuantity($quantityAvailable);
        }
      }
    }
  }
}
