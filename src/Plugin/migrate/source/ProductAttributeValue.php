<?php

namespace Drupal\Dolibarr\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "dolibarr_product_attribute_value_sql",
 *   source_module = "dolibarr",
 * )
 */
class ProductAttributeValue extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'llxtk_product_attribute_value' table.
    $query = $this->select('product_attribute_value', 'product_attrib_vals')
      ->fields('product_attrib_vals', [
          'rowid',
          'fk_product_attribute',
          'ref',
          'value',
        ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'rowid'                => $this->t('rowid'),
      'fk_product_attribute' => $this->t('fk_product_attribute'),
      'ref'                  => $this->t('ref'),
      'value'                => $this->t('value'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */

/*
alias explanation:

The most common setting passed along to the ID definition
is table 'alias', used by the SqlBase source plugin in
order to distinguish between ambiguous column names - for
example, when a SQL source query joins two tables with
the same column names.
*/
  public function getIds() {
    return [
      'rowid' => [
        'type' => 'integer',
        'alias' => 'product_attrib_vals',
      ],
    ];
  }
}

