<?php

namespace Drupal\Dolibarr\Plugin\migrate\source;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Returns filenames for any .png, jpg files attached to products in dolibarr
 *
 * @MigrateSource(
 *   id = "dolibarr_product_image_sql",
 *   source_module = "dolibarr",
 * )
 */
class ProductImage extends SqlBase {
  /**
   * {@inheritdoc}
   */
  public function query() {

    \Drupal::logger('dolibarr_migrate_products')->notice("Querying product images.");
    $query = $this->select('ecm_files', 'product_image')
      ->fields('product_image', [
          'rowid',
          'date_c',
          'filename',
          'filepath',
          'src_object_id',
        ]);

    //grab everything to the right of / for produit/BR-TIEDY as ref
    $query->addExpression("SUBSTRING_INDEX(product_image.filepath, '/', -1)", 'ref');
    //only grab products
    $query->condition('src_object_type', 'product', '=');

    // Create the orConditionGroup
    $orGroup = $query->orConditionGroup()
      ->condition('filename', '%.jpg', 'LIKE')
      ->condition('filename', '%.JPG', 'LIKE')
      ->condition('filename', '%.jpeg', 'LIKE')
      ->condition('filename', '%.JPEG', 'LIKE')
      ->condition('filename', '%.png', 'LIKE')
      ->condition('filename', '%.PNG', 'LIKE');

    // Add the group to the query.
    $query->condition($orGroup);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  //defines an associative array with keys as field names and their contents. but this doesn't actually do anything in the migration
  public function fields() {
    $fields = [
      'rowid'                => $this->t('rowid'),
      'ref'                  => $this->t('ref'),
      'datec'                => $this->t('date_c'),
      'filename'             => $this->t('filename'),
      'src_object_id'        => $this->t('src_object_id'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */

/*
alias explanation:

The most common setting passed along to the ID definition
is table 'alias', used by the SqlBase source plugin in
order to distinguish between ambiguous column names - for
example, when a SQL source query joins two tables with
the same column names.
*/
  public function getIds() {
    return [
      'ref' => [
        'type' => 'string',
        'alias' => 'product_image',
      ],
      'filename' => [
        'type' => 'string',
        'alias' => 'product_image',
      ],
    ];
  }
}
