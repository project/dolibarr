<?php

namespace Drupal\Dolibarr\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

use Drupal\Core\Database\Database;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "dolibarr_product_term_sql",
 *   source_module = "dolibarr",
 * )
 */
class ProductTerm extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {

    // Source data is queried from 'llxtk_product' table.
    $query = $this->select('categorie_product', 'category_product')
      ->fields('category_product', [
          'fk_product',
        ]);

    $query->innerJoin('product_extrafields', 'product_extrafields', 'category_product.fk_product = product_extrafields.fk_object');
    $query->condition('product_extrafields.online_store_sale', 1, '=');

    $query->groupBy('fk_product');
    $query->addExpression('GROUP_CONCAT(fk_categorie)', 'tax_concatted');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'fk_categorie'         => $this->t('fk_categorie'),
      'fk_product'           => $this->t('fk_product'),
      'tax_concatted'     => $this->t('tax_concatted'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */

/*
alias explanation:

The most common setting passed along to the ID definition
is table 'alias', used by the SqlBase source plugin in
order to distinguish between ambiguous column names - for
example, when a SQL source query joins two tables with
the same column names.
*/
  public function getIds() {
    return [
      'fk_product' => [
        'type' => 'integer',
        'alias' => 'category_product',
      ],
    ];
  }
}
