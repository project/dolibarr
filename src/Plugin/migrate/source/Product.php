<?php

namespace Drupal\Dolibarr\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "dolibarr_product_sql",
 *   source_module = "dolibarr",
 * )
 */
class Product extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {

    // Source data is queried from 'llxtk_product' table.
    $query = $this->select('product', 'product')
      ->fields('product', [
          'rowid',
          'label',
          'ref',
          'datec',
          'fk_product_type',
          'description',
          'note_public',
        ]);
    #need to inner join product_extrafields with product table where fk_object = rowid on product to get online_store_sale
    $query->innerJoin('product_extrafields', 'product_extrafields', 'product.rowid = product_extrafields.fk_object');
    $query->condition('product_extrafields.online_store_sale', 1, '=');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'rowid'                => $this->t('rowid'),
      'label'                => $this->t('label'),
      'ref'                  => $this->t('ref'),
      'datec'                => $this->t('datec'),
      'fk_product_type'      => $this->t('fk_product_type'),
      'description'          => $this->t('description'),
      'note_public'          => $this->t('note_public'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */

/*
alias explanation:

The most common setting passed along to the ID definition
is table 'alias', used by the SqlBase source plugin in
order to distinguish between ambiguous column names - for
example, when a SQL source query joins two tables with
the same column names.
*/
  public function getIds() {
    return [
      'rowid' => [
        'type' => 'integer',
        'alias' => 'product',
      ],
    ];
  }
}
