<?php

namespace Drupal\Dolibarr\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "dolibarr_product_variation_sql",
 *   source_module = "dolibarr",
 * )
 */

class ProductVariation extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {

    // Source data is queried from 'llxtk_product' table.
    $query = $this->select('product', 'product_variation')
      ->fields('product_variation', [
          'rowid',
          'label',
          'ref',
          'datec',
          'price',
          'weight',
          'weight_units',
          'length',
          'length_units',
          'width',
          'width_units',
          'height',
          'height_units',
          'surface',
          'surface_units',
          'volume',
          'volume_units',
        ]);
		
    $query->innerJoin('product_extrafields', 'product_extrafields', 'product_variation.rowid = product_extrafields.fk_object');
    $query->condition('product_extrafields.online_store_sale', 1, '=');
    $query->addField('product_extrafields', 'online_store_sale', 'online_store_sale');
    $query->leftJoin('multicurrency','multicurrency','multicurrency.rowid = 1');
    $query->addField('multicurrency', 'code', 'multicurrency_code_1');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'rowid'                => $this->t('rowid'),
      'label'                => $this->t('label'),
      'ref'                  => $this->t('ref'),
      'datec'                => $this->t('datec'),
      'price'                => $this->t('price'),
      'online_store_sale'    => $this->t('online_store_sale'),
      'multicurrency_code_1' => $this->t('multicurrency_code_1'),
      'weight'               => $this->t('weight'),
      'weight_units'         => $this->t('weight_units'),
      'length'               => $this->t('length'),
      'length_units'         => $this->t('length_units'),
      'width'                => $this->t('width'),
      'width_units'          => $this->t('width_units'),
      'height'               => $this->t('height'),
      'height_units'         => $this->t('height_units'),
      'surface'              => $this->t('surface'),
      'surface_units'        => $this->t('surface_units'),
      'volume'               => $this->t('volume'),
      'volume_units'         => $this->t('volume_units'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */

/*
alias explanation:

The most common setting passed along to the ID definition
is table 'alias', used by the SqlBase source plugin in
order to distinguish between ambiguous column names - for
example, when a SQL source query joins two tables with
the same column names.
*/
  public function getIds() {
    return [
      'ref' => [
        'type' => 'string',
        'alias' => 'product_variation',
      ],
    ];
  }
}