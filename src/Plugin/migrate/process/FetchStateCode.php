<?php

namespace Drupal\Dolibarr\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Creates a address/administrative_area array from the input value.
 *
 * Returns state code as an array.
 *
 * Example:
 * @code
 * address/administrative_area:
 *   plugin: dolibarr_fetch_state
 *   source:
 *     - code
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "dolibarr_fetch_state"
 * )
 */

 
class FetchStateCode extends ProcessPluginBase {

    use StringTranslationTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
    protected $httpClient;


  	/**
   	* {@inheritdoc}
   	*/
  	public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
		
		$apiToken = getAPIToken();
		$doliResponse = \Drupal::httpClient()->get(getAPIUrlBase() . "/setup/dictionary/states/$value", [
			'headers' => ['DOLAPIKEY' => $apiToken],
			'http_errors' => false,
		  ]);
	  
		$stateInfo = json_decode((string) $doliResponse->getBody(), true);

		if (isset($stateInfo['code'])) {
			$new_value = $stateInfo['code'];
			return $new_value;
		}

		return;
  }

}
