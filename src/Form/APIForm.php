<?php

namespace Drupal\dolibarr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a simple form for entering an API key and URL.
 */
class APIForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'APIForm';
    }
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $config = $this->configFactory()->getEditable('dolibarr.settings');

      $form['api_information'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . $this->t('API Information') . '</h2>',
      ];

      $form['api_key'] = [
        '#type' => 'password',
        '#title' => $this->t('API Key'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr API key.'),
      ];
  
      $form['api_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('API URL'),
        '#default_value' => $config->get('dolibarr_settings.api_url'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr API URL.'),
      ];

      $form['dolibarr_details'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . $this->t('Dolibarr Details') . '</h2>',
      ];

      $form['dolibarr_account_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Dolibarr Account ID'),
        '#default_value' => $config->get('dolibarr_settings.dolibarr_account_id'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr payment account ID. This defaults to 1 which is the default account ID in Dolibarr.'),
      ];

      $form['guest_account'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Dolibarr Guest Account ID'),
        '#default_value' => $config->get('dolibarr_settings.guest_account'),
        '#required' => FALSE,
        '#description' => $this->t('This is the Dolibarr Third Party ID that will be used for all accounts that are not already attached to a third party in Dolibarr.'),
      ];

      $form['product_tags'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Product Tags'),
        '#default_value' => $config->get('dolibarr_settings.product_tags'),
        '#required' => FALSE,
        '#description' => $this->t('These are the product tags that will be pulled from Dolibarr. Must be in this format: ["Top Tag A","Top Category A","Top Category B","Misc Category C"].'),
      ];

      $form['warehouse'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Dolibarr Warehouse'),
        '#default_value' => $config->get('dolibarr_settings.warehouse'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr warehouse ID. This is the warehouse that will be used for all orders.'),
      ];

      $form['product_codes'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . $this->t('Dolibarr Product Codes') . '</h2>',
      ];

      $form['order_fee_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Order Fee Product Code'),
        '#default_value' => $config->get('dolibarr_settings.order_fee_code'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr product code for order processing fees.'),
      ];

      $form['shipping_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Shipping Product Code'),
        '#default_value' => $config->get('dolibarr_settings.shipping_code'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr product code for shipping.'),
      ];

      $form['tax_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Tax Product Code'),
        '#default_value' => $config->get('dolibarr_settings.tax_code'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the Dolibarr product code for tax.'),
      ];

      $form['Other'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . $this->t('Other') . '</h2>',
      ];

      $form['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Email'),
        '#default_value' => $config->get('dolibarr_settings.email'),
        '#required' => FALSE,
        '#description' => $this->t('Enter the email address that will be used for receiving error emails'),
      ];

      $form['processing_fee'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Include processing fee'),
        '#default_value' => $config->get('dolibarr_settings.processing_fee'),
      ];

      $form['processing_fee_percentage'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Processing fee percentage'),
        '#default_value' => $config->get('dolibarr_settings.processing_fee_percentage'),
        '#states' => [
          'visible' => [
            ':input[name="processing_fee"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="processing_fee"]' => ['checked' => TRUE],
          ],
        ],
        '#description' => $this->t('Enter the processing fee percentage.'),
      ];
  
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ];
  
      return $form;
    }
    
    /**
     * Submit handler for the simple form.
     * 
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $api_key = $form_state->getValue('api_key');
      $api_url = $form_state->getValue('api_url');
      $dolibarr_account_id = $form_state->getValue('dolibarr_account_id');
      $email = $form_state->getValue('email');
      $guest_account = $form_state->getValue('guest_account');
      $order_fee_code = $form_state->getValue('order_fee_code');
      $processing_fee = $form_state->getValue('processing_fee');
      $processing_fee_percentage = $form_state->getValue('processing_fee_percentage');
      $product_tags = $form_state->getValue('product_tags');
      $shipping_code = $form_state->getValue('shipping_code');
      $tax_code = $form_state->getValue('tax_code');
      $warehouse = $form_state->getValue('warehouse');

      if (!$processing_fee) {
        $processing_fee_percentage = 0;
      }
    
      // Save the API key and URL to the configuration.
      $config = $this->configFactory()->getEditable('dolibarr.settings');

      if ($api_key != '') {
        $config->set('dolibarr_settings.api_key', $api_key)->save();
      }

      $config->set('dolibarr_settings.api_url', $api_url)->save();
      $config->set('dolibarr_settings.dolibarr_account_id', $dolibarr_account_id)->save();
      $config->set('dolibarr_settings.email', $email)->save();
      $config->set('dolibarr_settings.guest_account', $guest_account)->save();
      $config->set('dolibarr_settings.order_fee_code', $order_fee_code)->save();
      $config->set('dolibarr_settings.processing_fee', $processing_fee)->save();
      $config->set('dolibarr_settings.processing_fee_percentage', $processing_fee_percentage)->save();
      $config->set('dolibarr_settings.product_tags', $product_tags)->save();
      $config->set('dolibarr_settings.shipping_code', $shipping_code)->save();
      $config->set('dolibarr_settings.tax_code', $tax_code)->save();
      $config->set('dolibarr_settings.warehouse', $warehouse)->save();

      $this->messenger()->addMessage('Settings have been saved.');
    
      $form_state->setRedirect('<current>');
    }
}